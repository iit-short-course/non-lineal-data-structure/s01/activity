# 1. Create an activity folder and an activity.py inside of it.

# 2. Mimic the given terminal output by creating the following "list".

	#Activity No. 1 
	# a. Create a list named "numbers" that will contain five(5) integer values.
numbers=[1,2,3,4,5]
print(f'list of numbers: {numbers}')


	# b. Create a list named "meals" that will contain three(3) string values.
meals=['spam','Spam','SPAM!']	

# Activity No. 3
# 3. Using the common "list literals and operators", mimic the given terminal output.
print('list literals and operators ') 
	# a. Create a "messages" list and insert a string value "Hi!" four times using the repeat operator.
messages=["Hi!"]
messages=messages*4
print(messages)	

# b. Perform the following operations using the "numbers" list.

	# b.1. Print the length of the "numbers" list
print(f'length of the number list: {len(numbers)}')	

	# b.2. Removed the last item on the "numbers" list.
print(f'the last item removed from the list: {numbers.pop()}')

	# b.3. Reverse the sorting order of the "numbers" list items.
numbers.reverse()	
print(f'the list in reverse order is : {numbers}')

# c. Perform the following operations using the "meals" list.
print(f"list literals and operations using 'meals' list") 
	# c.1. Using the indexing operation, print the item "SPAM!" from the "meals" list.
print(f'using indexing operation:{meals[2]}')	

	# c.2. Using the negative indexing operation, print the item "Spam" from the "L" list.
print(f'using negative indexing:{meals[-2]}')	

	# c.3. Reassign the item "Spam" with "eggs" and print the modified "meals" list.
meals.insert(1,"eggs")
print(f'modified list of meals: {meals}')	

	# c.4. Add another list item with a value of "bacon" and print the modified "meals" list.
meals.append("bacon")
print(f'added a new item in a list:{meals}')	

	# c.5. Arrange your list items using the sort method and print the modified "meals" list.
meals.sort()
print(f'modified list after sort method : {meals}')	
# Activity No. 4 (Modified this one)
# 4. Create a dictionary named "recipe" with an initial key of "eggs" and a value of 3.
recipe={
"eggs":3
}
print(f'created recipe dictionary:{recipe}')

	# Activity No. 5
	# a. Add the following keys and values in the recipe: 
		# "spam":2, "ham":1, "brunch": bacon
	# and print the "recipe" to view changes. 
recipe["span"]=2
recipe["ham"]=1
recipe["brunch"]="bacon"
print(recipe)


	# b. Update the "ham" key value with the following:
		# ["grill", "bake", "fry"]

recipe.update({"ham":{"grill", "bake", "fry"}})	
print(recipe)	
	# and print the "recipe" to view changes. 
	# c. Delete the "eggs" key in the dictionary and print the "recipe" to view changes.

del recipe["eggs"]
print(recipe)

# Activity No.6
# 5. Using the provided "bob" dictionary, access and print the following key values in the terminal.

bob = 	{'name': {'first': 'Bob', 'last': 'Smith'},
		 'age': 42,
		 'job': ['software', 'writing'],
		 'pay': (40000, 50000)}

		# a. Print the "name" values.
print(bob["name"])		

		# b. Print the "Smith" value in the "name" key.
print(bob["name"]['last'])		

		# c. Print the "5000" value in the "pay" key.
print(bob['pay'][1])		


# 6. Using the provided "numeric" tuple, access and print the following items in the terminal.

numeric=('twelve', 3.0, [11,22,33])
print(f'Given numeric tupple: {numeric}')
# a. Print the item equal to "3.0"
print(numeric[1])

# b. Print the item equal to "22"
print(numeric[2][1])
